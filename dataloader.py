from torch.utils.data import Dataset,DataLoader

class XXDataset(Dataset):

    def __init__(self,folder_path,split,encoder):
        self.dataset = _load_data(folder_path,split) #加载模型
        self.encoder = encoder #编码

        self.cache = {} #可以存放，只需处理一次数据
    
    def _load_data(self,path, split)
        # 加载原始数据
        # 比如pd.read_csv ,with open(xxx)等等, 如果多个文件可以搞一个dataset=[],然后一个一个打开往dataset加东西，返回dataset
        return dataset

    def __len__(self):
        return len(self.dataset)
    
    def __getitem__(self,idx):
        # todo 根据索引idx取数据,处理数据并输出成模型需要的形状
        if index in self.cache: #如果有，直接从cache里取出
            coors, feats, label = self.cache[index]
        else:
            #todo 处理数据
            self.cache[index] = (coors, feats, label)
        return coors, feats, label
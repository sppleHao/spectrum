import sys
import torch 
import torch.nn as nn 
import time
import random
import numpy as np
import math

class RNet(nn.Module):
    def __init__(self):
        super(RNet,self).__init__()
        self.baseline = self.build_modle()
        self.fc1 = nn.Sequential(nn.Linear(160,256),nn.BatchNorm1d(256),nn.ReLU(True))
        self.fc2 = nn.Sequential(nn.Linear(256,128),nn.BatchNorm1d(128),nn.ReLU(True))
        self.fc3 = nn.Sequential(nn.Linear(128,64),nn.BatchNorm1d(64),nn.ReLU(True))
        self.fc4 = nn.Linear(64,12)

    def forward(self,x):
        x = x.unsqueeze(1)
        for i in range(len(self.baseline)):
            x = self.baseline[i](x)
        x = x.view(x.size(0),-1)
        x = self.fc1(x)
        x = self.fc2(x)
        x = self.fc3(x)
        x = self.fc4(x)
        return x

    def build_modle(self,inres=1,conv_filters=[32,64,128,64,32]):
        convs = []
        convs.append(nn.Sequential(nn.Conv1d(in_channels=inres,out_channels=conv_filters[0],kernel_size=3),nn.ReLU(True),nn.MaxPool1d(2),nn.BatchNorm1d(32)))
        for i in range(len(conv_filters)-1):
            convs.append(nn.Sequential(nn.Conv1d(in_channels=conv_filters[i],out_channels=conv_filters[i+1],kernel_size=3),nn.ReLU(True),nn.MaxPool1d(2),nn.BatchNorm1d(conv_filters[i+1])))
        return nn.ModuleList(convs)
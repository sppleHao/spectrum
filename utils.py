import sys
import torch 
import torch.nn as nn 
import torch.nn.functional as F
import time
import random
import numpy as np
from model import BiNet,FNet,RNet
import matplotlib.pyplot as plt
import pandas as pd


def init_weights(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)


def adjust_learning_rate(optimizer, learningrate,epoch):
    """Sets the learning rate to the initial LR decayed by 80% every 10 epochs"""
    lr = learningrate * (0.8 ** (epoch // 10))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr

def train(device,train_loader,valid_loader,optimizer,epoch_num,learningrate,network):
        assert network == 'BiNet' or network == 'FNet' or network == 'RNet'
        model = BiNet()
        model.apply(init_weights) #初始化权重 
        criterion = nn.MSELoss(reduction='sum')
        if optimizer =='SGD':
            optim = torch.optim.SGD(model.parameters(),lr=learningrate,momentum=0.9)
        elif optimizer == 'Adam':
            optim = torch.optim.Adam(model.parameters(),lr=learningrate)
        model = model.cuda().double()
        print('----------Preparing model...')
        train_data_size = 0
        valid_data_size = 0
        for i,batch in enumerate(train_loader):
            train_data_size += batch['spec'].size(0)
        for i,batch in enumerate(valid_loader):
            valid_data_size += batch['spec'].size(0)

        train_loss_list = []

        begin_time = time.time()
        print('----------Begin to train a initialized '+network+' model at '+time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())+'\r')
        min_loss = 0
        for epoch in range(epoch_num):    
            print('---------------------------------------------------------------------')  
            model.train()
            running_loss = 0.0
            for i,batch in enumerate(train_loader):
                sys.stdout.write('----------Epoch: {}/{} Batch: {}/{}'.format(epoch+1,epoch_num,i+1,len(train_loader))+'\r')
                spec = batch['spec'].float().cuda().double()
                param = batch['param'].float().cuda().double()

                optim.zero_grad()                
                if network == 'FNet':
                    outputs = model.FNet(param)
                    loss = criterion(outputs,spec)
                elif network == 'RNet':
                    outputs = model.RNet(spec)
                    loss = criterion(outputs,param)
                elif network == 'BiNet':    
                    predicted_param,predicted_spec= model(spec)
                    F_loss = criterion(predicted_spec,spec)
                    R_loss = criterion(predicted_param,param)
                    loss = F_loss + R_loss

                loss.backward()
                optim.step()
                running_loss += loss.item()  
            epoch_loss = running_loss / train_data_size
            print('/n')
            print('----------Traing loss: {}'.format(epoch_loss)+'\r')
            train_loss_list.append(epoch_loss)
            
            adjust_learning_rate(optim,learningrate,epoch+1)
            #evaluate phase
            model.eval()
            runningloss = 0
            print('----------Computing valid loss...')
            for i,batch in enumerate(valid_loader):
                spec = batch['spec'].float().cuda().double()
                param = batch['param'].float().cuda().double()
                with torch.no_grad():
                    if network == 'FNet':
                        outputs = model.FNet(param)
                        loss = criterion(outputs,spec)
                    elif network == 'RNet':
                        outputs = model.RNet(spec)
                        loss = criterion(outputs,param)
                    elif network == 'BiNet':
                        predicted_param,predicted_spec = model(spec)
                        F_loss = criterion(predicted_spec,spec)
                        R_loss = criterion(predicted_param,param)
                        loss = F_loss + R_loss
                    runningloss += loss.item()
            runningloss = runningloss / valid_data_size
            print('----------Valid Loss: {}'.format(runningloss)+'\r')
            
            if epoch == 0 :
                torch.save(model.state_dict(),'model_saved/'+str(int(begin_time))+'.pt')      #保存模型，文件名为训练起始时间
                min_loss = runningloss
                print('----------Min loss:{} Valid loss:{}, Model was saved in dir /model_saved/'.format(min_loss,runningloss)+str(int(begin_time))+'.pt') 
            else :
                if runningloss < min_loss:
                    torch.save(model.state_dict(),'model_saved/'+str(int(begin_time))+'.pt')      #保存模型，文件名为训练起始时间
                    print('----------Min loss:{} Valid loss:{}, Model was saved in dir /model_saved/'.format(min_loss,runningloss)+str(int(begin_time))+'.pt') 
                    min_loss = runningloss

        print('----------Finish training at ' + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))

        numpy.savetxt("train.csv", np.array(train_loss_list), delimiter=',')

def retrain(modelname,device,train_loader,valid_loader,optimizer,epoch_num,learningrate,network):
        assert network == 'BiNet' or network == 'FNet' or network == 'RNet'
        model = BiNet()
        model.load_state_dict(torch.load('model_saved/'+modelname+'.pt'))
        model = model.cuda()
        criterion = nn.MSELoss(reduction='sum')
        if optimizer =='SGD':
            optim = torch.optim.SGD(model.parameters(),lr=learningrate,momentum=0.9)
        elif optimizer == 'Adam':
            optim = torch.optim.Adam(model.parameters(),lr=learningrate)

        print('----------Preparing model...')
        train_data_size = 0
        valid_data_size = 0
        for i,batch in enumerate(train_loader):
            train_data_size += batch['spec'].size(0)
        for i,batch in enumerate(valid_loader):
            valid_data_size += batch['spec'].size(0)

        min_loss = 0
        model.eval()
        runningloss = 0
        for i,batch in enumerate(valid_loader):
            spec = batch['spec'].float().cuda()
            param = batch['param'].float().cuda()
            with torch.no_grad():
                if network == 'FNet':
                    outputs = model.FNet(param)
                    loss = criterion(outputs,spec)
                elif network == 'RNet':
                    outputs = model.RNet(spec)
                    loss = criterion(outputs,param)
                elif network == 'BiNet':
                    predicted_param,predicted_spec = model(spec)
                    F_loss = criterion(predicted_spec,spec)
                    R_loss = criterion(predicted_param,param)
                    loss = F_loss + R_loss
                runningloss += loss.item()
        min_loss = runningloss / valid_data_size
        print('----------Min Loss: {}'.format(min_loss)+'\r')


        print('----------Begin to retrain '+ network +' of '+ modelname + ' at ' +time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())+'\r')
        for epoch in range(epoch_num):  
            print('---------------------------------------------------------------------')  
            #train phase            
            model.train()
            running_loss = 0.0 
            for i,batch in enumerate(train_loader):
                sys.stdout.write('----------Epoch: {}/{} Batch: {}/{}'.format(epoch+1,epoch_num,i+1,len(train_loader))+'\r')
                spec = batch['spec'].float().cuda()
                param = batch['param'].float().cuda()
                optim.zero_grad()
                if network == 'FNet':
                    outputs = model.FNet(param)
                    loss = criterion(outputs,spec)
                elif network == 'RNet':
                    outputs = model.RNet(spec)
                    loss = criterion(outputs,param)
                elif network == 'BiNet':
                    predicted_param,predicted_spec = model(spec)
                    F_loss = criterion(predicted_spec,spec)
                    R_loss = criterion(predicted_param,param)
                    loss = F_loss + R_loss
                loss.backward()
                optim.step()
                running_loss += loss.item()  
            epoch_loss = running_loss / train_data_size
            print('/n')
            print('----------Training loss: {}'.format(epoch_loss)+'\r')
            
            adjust_learning_rate(optim,learningrate,epoch+1)
            #evaluate phase
            model.eval()
            runningloss = 0
            for i,batch in enumerate(valid_loader):
                spec = batch['spec'].float().cuda()
                param = batch['param'].float().cuda()
                with torch.no_grad():
                    if network == 'FNet':
                        outputs = model.FNet(param)
                        loss = criterion(outputs,spec)
                    elif network == 'RNet':
                        outputs = model.RNet(spec)
                        loss = criterion(outputs,param)
                    elif network == 'BiNet':
                        predicted_spec,predicted_param = model(param)
                        F_loss = criterion(predicted_spec,spec)
                        R_loss = criterion(predicted_param,param)
                        loss = F_loss + R_loss
                    runningloss += loss.item()
            runningloss = runningloss / valid_data_size
            print('----------Valid Loss: {}'.format(runningloss)+'\r')
            if runningloss < min_loss:
                    torch.save(model.state_dict(),'model_saved/'+str(modelname)+'.pt')      #保存模型，文件名为训练起始时间
                    print('----------Min loss:{} Valid loss:{}, Model was saved in dir /model_saved/'.format(min_loss,runningloss)+str(modelname)+'.pt') 
                    min_loss = runningloss

        print('----------Finish retraining model '+ network +' of '+ modelname+ ' at ' + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))


def test(modelname,device,test_loader):
    model = BiNet()
    model.load_state_dict(torch.load('model_saved/'+modelname+'.pt'))
    model = model.cuda()
    criterion = nn.MSELoss(reduction='sum')
    model.eval()
    F_total_loss = 0
    R_total_loss = 0
    total_loss = 0
    data_size = 0

    print('----------Begin to test model '+ modelname+ ' at ' + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
    for i,batch in enumerate(test_loader):
        sys.stdout.write('----------Test Batch: {}/{}'.format(i+1,len(test_loader))+'\r')
        spec = batch['spec'].float().cuda()
        param = batch['param'].float().cuda()
        data_size += spec.size(0)
        with torch.no_grad():
            predicted_param,predicted_spec = model(spec)
            F_loss = criterion(predicted_spec,spec)
            R_loss = criterion(predicted_param,param)
            loss = F_loss + R_loss

            F_total_loss += F_loss.item()
            R_total_loss += R_loss.item()
            total_loss += loss.item()
    total_loss = total_loss / data_size
    F_total_loss = F_total_loss / data_size
    R_total_loss = R_total_loss /data_size
    print('----------Total Loss: {}   F_loss:{}   R_loss:{}'.format(total_loss,F_total_loss,R_total_loss)+'\r')
    print('----------Finish testing model '+ modelname+ ' at ' + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))


def demo(modelname,test_data,mean,std):
    model = BiNet()
    model.load_state_dict(torch.load('model_saved/'+modelname+'.pt',map_location='cpu'))
    model.eval()

    print('----------Begin to demo '+ modelname+ ' at ' + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))

    #test_data_size = len(test_data)
    #sample_list = random.sample([x for x in range(test_data_size)],20)
    sample_list = [x for x in range(20)]
    i=0
    for randindex in sample_list:
        sample = test_data[randindex]
        spec = torch.reshape(torch.tensor(sample['spec']).float(),(1,226))
        param = torch.reshape(torch.tensor(sample['param']).float(),(1,12))
        with torch.no_grad():
            predicted_spec = model.predict_spec(param)
            predicted_param = model.predict_param(spec)

        xaxis = np.linspace(11,20,226)
        predicted_spec = np.array(predicted_spec[0])
        spec = np.array(spec[0])

        
        plt.plot(xaxis,predicted_spec,label='predicted')
        plt.plot(xaxis,spec,label='truth')
        plt.legend()
        #plt.show()
        i=i+1
        plt.savefig('exp/{}.png'.format(i))
        plt.cla()


        predicted_param = np.array(predicted_param[0])
        predicted_param = predicted_param * std + mean
        param = np.array(param[0])
        param = param * std + mean
        df = pd.DataFrame({'truth':param,'predicted':predicted_param})
        print(df)


    print('----------Finish demo')

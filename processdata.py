import os,sys,re,random
import numpy as np
import pandas as pd
from torch.utils.data import Dataset,DataLoader

def transform_data_from(data_path,save_path):
    '''
    将data_path中原始数据处理后保存为train,test文件于data路径下
    '''
    dataset= []
    print('begin to read data...')
    for i in range(len(data_path)): #遍历每个文件夹
        print('reading files in ',data_path[i])
        feature_path = os.path.join(data_path[i],'spectrum_txt')
        label_path = os.path.join(data_path[i],'param')
        file_names = os.listdir(label_path) 
        for j in range(len(file_names)):   #遍历每条数据
            num = re.sub("\D", "", file_names[j])
            if os.path.exists(os.path.join(feature_path,"spec"+num+".txt")):#如果两个文件都存在
                sample = []
                with open(os.path.join(label_path,"param"+num+".txt"), "r") as f:  # 打开第i条数据的结构文件
                    data = f.readlines()  
                    for line in data:
                        line = float(line.strip('\n'))      
                        sample.append(line)
                with open(os.path.join(feature_path,"spec"+num+".txt"), "r") as f:  # 打开第i条数据的频谱文件
                    data = f.readlines()[8:]  #去除无效数据，前七条为log记录
                    for item in data:            #每条数据中的每行的波长索引
                        sample.append(float(item.split()[1]))     
                if len(sample)== 388:
                    dataset.append(sample)
    #训练/验证/测试集比例8：1：1
    data_num = len(dataset)
    train_num = round(0.8*data_num) 
    valid_num = round(0.9*data_num) 
    train_set = dataset[:train_num]
    valid_set = dataset[train_num:valid_num]
    test_set = dataset[valid_num:]
    print('finish reading data. ')
    print('begin to save as csv... ')
    #转化为nunmpy格式等待保存
    train_np_dataset = np.array(train_set)
    valid_np_dataset = np.array(valid_set)
    test_np_dataset = np.array(test_set)
    colname=[]                      #将numpy格式转化为csv格式并保存为文件
    for i in range(12):
        colname.append('structure'+str(i+1))
    for i in range(376):
        colname.append('wavelength'+str(5+0.04*i))

    #pandas加载并标准化结构参数。
    #光谱参数都位于0-1之间，所以暂不标准化。    
    train_pd = pd.DataFrame(train_np_dataset,columns=colname)
    valid_pd = pd.DataFrame(valid_np_dataset,columns=colname)
    test_pd = pd.DataFrame(test_np_dataset,columns=colname)
    columns = ['structure1','structure2','structure3','structure4','structure5','structure6','structure7','structure8','structure9','structure10','structure11','structure12']
    mean = []
    std = []
    for c in columns:
        mean.append(train_pd[c].mean())
        std.append(train_pd[c].std())
    
    #z-score标准化 
    train_pd[columns] = (train_pd[columns]-mean)/std
    valid_pd[columns] = (valid_pd[columns]-mean)/std
    test_pd[columns] = (test_pd[columns]-mean)/std

    #保存结构参数的mean,std
    file = open(os.path.join(save_path,'mean.txt'),'w')
    file.writelines([str(line)+'\n' for line in mean])
    file.close()
    file = open(os.path.join(save_path,'std.txt'),'w')
    file.writelines([str(line)+'\n' for line in std])
    file.close()
    train_pd.to_csv(os.path.join(save_path,'train_data.csv'))
    valid_pd.to_csv(os.path.join(save_path,'valid_data.csv'))
    test_pd.to_csv(os.path.join(save_path,'test_data.csv'))
    print('data have been saved at ',save_path)

def read_mean_std(data_path):
        curdir = os.getcwd()
        data_path = os.path.join(curdir,data_path)
        mean_path = os.path.join(data_path,'mean.txt')
        std_path = os.path.join(data_path,'std.txt')
        mean = []
        std = []
        with open(mean_path, "r") as f:
            data = f.readlines()   
            for line in data:
                line = float(line.strip('\n'))      
                mean.append(line)
        
        with open(std_path, "r") as f:  
            data = f.readlines()  
            for line in data:
                line = float(line.strip('\n'))      
                std.append(line)

        return mean,std


class NanoDataset(Dataset):

    def __init__(self,csv_file,transform=None):
        self.dataset = pd.read_csv(csv_file)
        self.transform = transform

    def __len__(self):
        return len(self.dataset)
    
    def __getitem__(self,idx):
        sample = {}
        sample['param'] = self.dataset.iloc[idx,1:13].values
        sample['spec'] = self.dataset.iloc[idx,163:389].values
        if self.transform:
            sample = self.transform(sample)
        return sample

class XXDataset(Dataset):

    def __init__(self,folder_path,split,encoder):
        self.dataset = _load_data(folder_path,split) #加载模型
        self.encoder = encoder #编码

        self.cache = {} #可以存放，只需处理一次数据
    
    def _load_data(self,path, split)
        # 加载原始数据
        # 比如pd.read_csv ,with open(xxx)等等, 如果多个文件可以搞一个dataset=[],然后一个一个打开往dataset加东西，返回dataset
        return dataset

    def __len__(self):
        return len(self.dataset)
    
    def __getitem__(self,idx):
        # todo 根据索引idx取数据,处理数据并输出成模型需要的形状
        if index in self.cache: #如果有，直接从cache里取出
            coors, feats, label = self.cache[index]
        else:
            #todo 处理数据
            self.cache[index] = (coors, feats, label)
        return coors, feats, label

if __name__ == "__main__":
    path = []
    curdir = os.getcwd()

    path.append(os.path.join(curdir,'data','original'))
    path.append(os.path.join(curdir,'data','matlab'))
    path.append(os.path.join(curdir,'data','spec'))
    transform_data_from(path,os.path.join(curdir,'data'))


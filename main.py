﻿import os,argparse,time,random
import torch
from processdata import transform_data_from,NanoDataset,read_mean_std
from torch.utils.data import DataLoader
from model import BiNet,FNet,RNet
from utils import train,test,demo,retrain
##命令行解析超参数

#训练相关参数
parser = argparse.ArgumentParser(description='DNN for NanoDesign')
parser.add_argument('--batch_size',type=int,default=512,help='sample number of each minibatch')
parser.add_argument('--epoch',type=int,default=100,help='epoch number of training')
parser.add_argument('--optimizer',type=str,default='Adam',help='Adam/SGD')
parser.add_argument('--lr',type=float,default=0.001,help='learning rate')
parser.add_argument('--shuffle',type=bool,default=True,help='shuffle training data before each epoch')

#操作选择
parser.add_argument('--use_gpu',type=bool,default=True,help='True for gpu')
parser.add_argument('--data_path',type=str,default='data',help='data source')
parser.add_argument('--mode',type=str,default='demo',help='train/retrain/test/demo')
parser.add_argument('--network',type=str,default='RNet',help='FNet,RNet,BiNet')
parser.add_argument('--model',type=str,default='1594002320',help='model for retrain, test and demo')

args = parser.parse_args()

#GPU和CPU的选择
if args.use_gpu and torch.cuda.is_available():
    device = torch.device('cuda:0')
    print('----------Use Gpu')
else :
    device = torch.device('cpu')

#读取数据
train_path = os.path.join(args.data_path,'train_data.csv')
valid_path = os.path.join(args.data_path,'valid_data.csv')
test_path = os.path.join(args.data_path,'test_data.csv')
mean,std = read_mean_std(args.data_path)

print('----------Begin to load dataset from csv file')
train_data = NanoDataset(train_path)
valid_data = NanoDataset(valid_path)
train_dataloader = DataLoader(train_data,batch_size=args.batch_size,shuffle=args.shuffle,pin_memory=True)
valid_dataloader = DataLoader(valid_data,batch_size=args.batch_size,shuffle=args.shuffle,pin_memory=True)
test_data = NanoDataset(test_path)
test_dataloader = DataLoader(test_data,batch_size=args.batch_size)
print('----------Data loaded')


if args.mode == 'train':
    train(device,train_dataloader,valid_dataloader,args.optimizer,args.epoch,args.lr,args.network)

if args.mode == 'retrain':
    retrain(args.model,device,train_dataloader,valid_dataloader,args.optimizer,args.epoch,args.lr,args.network)

if args.mode == 'test':
    test(args.model,device,test_dataloader)

if args.mode == 'demo':
    demo(args.model,test_data,mean,std)

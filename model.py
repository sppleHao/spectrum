import sys
import torch 
import torch.nn as nn 
import time
import random
import numpy as np
import math

class NTN(nn.Module):
    def __init__(self, in_features, out_features, bias=False):
        super(NTN,self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.W = nn.Parameter(torch.Tensor(out_features,in_features,in_features))
        self.V = nn.Parameter(torch.Tensor(out_features,in_features))
        if bias:
            self.bias = nn.Parameter(torch.Tensor(out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def forward(self,x):
        first = nn.functional.bilinear(x,x,self.W)
        second = nn.functional.linear(x,self.V)
        if self.bias is not None:
            out = first + second + self.bias
        else:
            out = first + second
        return out

    def reset_parameters(self):
        nn.init.kaiming_uniform_(self.W, a=math.sqrt(5))
        nn.init.kaiming_uniform_(self.V, a=math.sqrt(5))

class BaseLine(nn.Module):
    def __init__(self,inres,outres):
        super(BaseLine,self).__init__()
        self.conv1 = nn.Conv1d(in_channels=inres,out_channels=outres, padding=1,kernel_size=3)
        self.bn1 =nn.BatchNorm1d(outres)
        
        self.conv2 = nn.Conv1d(in_channels=outres,out_channels=outres*2, padding=1,kernel_size=3)
        self.bn2 = nn.BatchNorm1d(outres*2)
        
        self.conv3 = nn.Conv1d(in_channels=outres*2,out_channels=outres, padding=1,kernel_size=3)
        self.bn3 = nn.BatchNorm1d(outres)
        
        self.pool = nn.MaxPool1d(2)
        self.relu = nn.ReLU(True)

    def forward(self,x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.pool(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.pool(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)
        out = self.relu(out)

        return out

class RNet(nn.Module):
    def __init__(self):
        super(RNet,self).__init__()
        self.baseline = BaseLine(1,32)
        self.conv1 = nn.Sequential(nn.Conv1d(in_channels=32, out_channels=64, kernel_size=1),nn.ReLU(True),nn.BatchNorm1d(64))
        self.conv2 = nn.Sequential(nn.Conv1d(in_channels=64, out_channels=64, kernel_size=3,padding=1),nn.ReLU(True),nn.BatchNorm1d(64))
        self.fc=nn.Sequential(nn.Linear(3584,12))
    def forward(self,x):
        x = x.unsqueeze(1)
        x = self.baseline(x)
        x = self.conv1(x)
        x = self.conv2(x)
        x = x.view(x.size(0),-1)
        x = self.fc(x)
        return x

'''
class RNet(nn.Module):
    def __init__(self):
        super(RNet,self).__init__()
        self.layer1=nn.Sequential(nn.Conv1d(in_channels=1, out_channels=16, kernel_size=15),nn.ReLU(True))
        self.layer2=nn.Sequential(nn.Conv1d(in_channels=25, out_channels=50, kernel_size=15),nn.MaxPool1d(3),nn.ReLU(True))
        self.layer3=nn.Sequential(nn.Conv1d(in_channels=50, out_channels=25, kernel_size=10),nn.ReLU(True))
        self.layer4=nn.Sequential(nn.Conv1d(in_channels=25, out_channels=10, kernel_size=10),nn.MaxPool1d(2),nn.ReLU(True))
        self.layer5=nn.Sequential(nn.Linear(240,200),nn.BatchNorm1d(200),nn.ReLU(True))
        self.layer6=nn.Sequential(nn.Linear(200,128),nn.BatchNorm1d(128),nn.ReLU(True))
        self.layer7=nn.Sequential(nn.Linear(128,64),nn.BatchNorm1d(64),nn.ReLU(True))
        self.layer8=nn.Sequential(nn.Linear(64,12))
    
    def forward(self,x):
        # N x L 转化成 N x 1 x L 
        x = x.unsqueeze(1)
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        #第三层卷积后转化为 N X 240
        x = x.view(x.size()[0],240)
        x = self.layer5(x)
        x = self.layer6(x)
        x = self.layer7(x)
        x = self.layer8(x)
        return x
'''

class FNet(nn.Module):
    def __init__(self):
        super(FNet,self).__init__()
        self.ntn = NTN(12,128)
        self.layer1=nn.Sequential(nn.Linear(128,128),nn.BatchNorm1d(128),nn.ReLU(True))
        self.layer2=nn.Sequential(nn.Linear(128,128),nn.BatchNorm1d(128),nn.ReLU(True))
        self.layer3=nn.Sequential(nn.Linear(128,256),nn.BatchNorm1d(256),nn.ReLU(True))
        self.layer4=nn.Sequential(nn.Linear(256,512),nn.BatchNorm1d(512),nn.ReLU(True))
        self.layer5=nn.Sequential(nn.Linear(512,1024),nn.BatchNorm1d(1024),nn.ReLU(True))
        self.layer6=nn.Sequential(nn.Linear(1024,2048),nn.BatchNorm1d(2048),nn.ReLU(True))
        self.layer7=nn.Sequential(nn.Linear(2048,4096),nn.BatchNorm1d(4096),nn.ReLU(True))
        self.layer8=nn.Sequential(nn.Linear(4096,4096),nn.BatchNorm1d(4096),nn.ReLU(True))
        self.layer9=nn.Sequential(nn.Linear(4096,226))
        
    def forward(self,x):
        x = self.ntn(x)
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = self.layer5(x)
        x = self.layer6(x)
        x = self.layer7(x)
        x = self.layer8(x)
        x = self.layer9(x)
        return x

    
class BiNet(nn.Module):
    def __init__(self):
        super(BiNet,self).__init__()
        self.FNet = FNet()
        self.RNet = RNet()
    
    def forward(self,spec):
        predicted_param = self.RNet(spec)
        predicted_spec = self.FNet(predicted_param)
        return predicted_spec
 
    def predict_spec(self,param):
        return self.FNet(param)

    def predict_param(self,spec):
        return self.RNet(spec)

class CNet(nn.Module):
    def __init__(self,decoder):
        super(CNet_1,self).__init__()
        self.encoder = BaseLine()
        self.decoder = decoder 
    
    def forward(self,spec):
        x = self.encoder(spec)
        predicted_param = self.decoder(x)
        return predicted_param
 
    def predict_spec(self,param):
        return self.FNet(param)

    def predict_param(self,spec):
        return self.RNet(spec)
    
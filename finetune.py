import os,sys,argparse,time,random
import torch
import torch.nn as nn
import torch.nn.functional as F
from processdata import transform_data_from,NanoDataset,read_mean_std
from torch.utils.data import DataLoader
from model import BiNet,FNet,RNet
import random
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def init_weights(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)

def fix_bn(m):
    classname = m.__class__.__name__
    if classname.find('BatchNorm') != -1:
        m.eval()

def adjust_learning_rate(optimizer, learningrate,epoch):
    """Sets the learning rate to the initial LR decayed by 80% every 10 epochs"""
    lr = learningrate * (0.8 ** (epoch // 50))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr

def train(modelname,device,train_loader,valid_loader,optimizer,epoch_num,learningrate,network):
        assert network == 'BiNet'
        model = BiNet()
        model.FNet.load_state_dict(torch.load('model/forward'+'.pt'))
        model.RNet.apply(init_weights)
        model = model.cuda()

        #for param in model.FNet.parameters():
            #param.requires_grad=False

        criterion = nn.MSELoss(reduction='sum')
        if optimizer =='SGD':
            optim = torch.optim.SGD(model.RNet.parameters(),lr=learningrate,momentum=0.9)
        elif optimizer == 'Adam':
            optim = torch.optim.Adam(model.RNet.parameters(),lr=learningrate)

        print('----------Before Finetune...')
        train_data_size = 0
        valid_data_size = 0
        for i,batch in enumerate(train_loader):
            train_data_size += batch['spec'].size(0)
        for i,batch in enumerate(valid_loader):
            valid_data_size += batch['spec'].size(0)

        model.eval()
        runningloss = 0
        for i,batch in enumerate(valid_loader):
            spec = batch['spec'].float().cuda()
            param = batch['param'].float().cuda()
            with torch.no_grad():
                if network == 'BiNet':
                    predicted_spec = model(spec)
                    loss = criterion(predicted_spec,spec)
                runningloss += loss.item()
        before_loss = runningloss / valid_data_size
        print('----------Min Loss: {}'.format(before_loss)+'\r')


        print('----------Begin to Finetune '+ network +' of '+ modelname + ' at ' +time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())+'\r')
        min_valid_loss=0
        for epoch in range(epoch_num):  
            print('---------------------------------------------------------------------')  
            #train phase            
            model.train()
            model.FNet.apply(fix_bn)
            running_loss = 0.0 
            for i,batch in enumerate(train_loader):
                sys.stdout.write('----------Epoch: {}/{} Batch: {}/{}'.format(epoch+1,epoch_num,i+1,len(train_loader))+'\r')
                spec = batch['spec'].float().cuda()
                param = batch['param'].float().cuda()
                if network == 'BiNet':
                    predicted_spec = model(spec)
                    loss = criterion(predicted_spec,spec)
                loss.backward()
                optim.step()
                running_loss += loss.item()  
            epoch_loss = running_loss / train_data_size
            print('/n')
            print('----------Training loss: {}'.format(epoch_loss)+'\r')
            
            adjust_learning_rate(optim,learningrate,epoch+1)
            #evaluate phase
            model.eval()
            valid_loss = 0
            for i,batch in enumerate(valid_loader):
                spec = batch['spec'].float().cuda()
                param = batch['param'].float().cuda()
                with torch.no_grad():
                    if network == 'BiNet':
                        predicted_spec = model(spec)
                        loss = criterion(predicted_spec,spec)
                    valid_loss += loss.item()
            valid_loss = valid_loss / valid_data_size
            #write in txt

            print('----------Valid Loss: {}'.format(valid_loss)+'\r')
            if valid_loss < min_valid_loss or min_valid_loss ==0:
                torch.save(model.state_dict(),'model_saved/finetune'+'.pt')      #保存模型，文件名为训练起始时间
                print('----------Min loss:{} Valid loss:{}, Model was saved in dir /model_saved/'.format(min_valid_loss,valid_loss)+'finetune.pt') 
                min_valid_loss = valid_loss

        print('----------Finish retraining model '+ network +' of '+ modelname+ ' at ' + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))

if __name__ == "__main__":
    data_path = 'data'
    batch_size=512
    shuffle = True
    #读取数据
    train_path = os.path.join(data_path,'train_data.csv')
    valid_path = os.path.join(data_path,'valid_data.csv')
    test_path = os.path.join(data_path,'test_data.csv')
    mean,std = read_mean_std(data_path)

    print('----------Begin to load dataset from csv file')
    train_data = NanoDataset(train_path)
    valid_data = NanoDataset(valid_path)
    train_dataloader = DataLoader(train_data,batch_size=batch_size,shuffle=shuffle,pin_memory=True)
    valid_dataloader = DataLoader(valid_data,batch_size=batch_size,shuffle=shuffle,pin_memory=True)
    test_data = NanoDataset(test_path)
    test_dataloader = DataLoader(test_data,batch_size=batch_size)
    print('----------Data loaded')

    print('----------Load Device')
    if torch.cuda.is_available():
        device = torch.device('cuda:0')
        print('----------Use Gpu')
    else :
        device = torch.device('cpu')
    
    train('finetune',device,train_dataloader,valid_dataloader,'Adam',200,0.001,'BiNet')

# Parameter-Spectrum Prediction
超材料参数-光谱预测
## Network Architecture
 ![caption](result/model.png) 
## Result
 ![caption](result/1.png) 
 ![caption](result/2.png)
 ![caption](result/3.png) 
 ![caption](result/4.png)
## Usage
### 1. 安装依赖

```sh
pip install -r requirements.txt
```
### 2. 克隆仓库到本地
```sh
git clone https://gitlab.com/sppleHao/spectrum.git
```

### 3. cd到目录下
```sh
cd folder-path
```

### 4. 分别训练正向模型（参数->光谱）和逆向模型（光谱->参数）

```sh
python train_cnet.py
```

```sh
python train_rnet.py
```

### 5. 联合训练，对正向模型finetune

```sh
python finetune.py
```

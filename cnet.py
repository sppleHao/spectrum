import sys
import torch 
import torch.nn as nn 
import time
import random
import numpy as np
import math

class BaseLine(nn.Module):
    def __init__(self,inres,outres):
        super(BaseLine,self).__init__()
        self.conv1 = nn.Conv1d(in_channels=inres,out_channels=outres, padding=1,kernel_size=3)
        self.bn1 =nn.BatchNorm1d(outres)
        
        self.conv2 = nn.Conv1d(in_channels=outres,out_channels=outres*2, padding=1,kernel_size=3)
        self.bn2 = nn.BatchNorm1d(outres*2)
        
        self.conv3 = nn.Conv1d(in_channels=outres*2,out_channels=outres, padding=1,kernel_size=3)
        self.bn3 = nn.BatchNorm1d(outres)
        
        self.pool = nn.MaxPool1d(2)
        self.relu = nn.ReLU(True)

    def forward(self,x):
        x = x.unsqueeze(1)

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.pool(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.pool(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)
        out = self.relu(out)

        return out
    
class Decoder_param(nn.Module):
    def __init__(self):
        super(Decoder_param,self).__init__()
        self.conv1 = nn.Sequential(nn.Conv1d(in_channels=32, out_channels=64, kernel_size=1),nn.ReLU(True),nn.BatchNorm1d(64))
        self.conv2 = nn.Sequential(nn.Conv1d(in_channels=64, out_channels=64, kernel_size=3,padding=1),nn.ReLU(True),nn.BatchNorm1d(64))
        self.fc1 = nn.Sequential(nn.Linear(3584,512),nn.BatchNorm1d(512),nn.ReLU(True))
        self.fc2 = nn.Sequential(nn.Linear(512,256),nn.BatchNorm1d(256),nn.ReLU(True))
        self.fc3 = nn.Linear(256,12)
    def forward(self,x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = x.view(x.size(0),-1)
        x = self.fc1(x)
        x = self.fc2(x)
        x = self.fc3(x)
        return x

class Decoder_spec(nn.Module):
    def __init__(self):
        super(Decoder_spec,self).__init__()
        self.fc1 = nn.Sequential(nn.Linear(1792,2048),nn.BatchNorm1d(2048),nn.ReLU(True))
        self.fc2=nn.Sequential(nn.Linear(2048,4096),nn.BatchNorm1d(4096),nn.ReLU(True))
        self.fc3=nn.Sequential(nn.Linear(4096,226))
    def forward(self,x):
        x = x.view(x.size(0),-1)
        x = self.fc1(x)
        x = self.fc2(x)
        x = self.fc3(x)
        return x

class CNet(nn.Module):
    def __init__(self,decoder):
        super(CNet,self).__init__()
        self.encoder = BaseLine(1,32)
        self.decoder = decoder 
    
    def forward(self,spec):
        x = self.encoder(spec)
        out = self.decoder(x)
        return out